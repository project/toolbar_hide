
(function($){
if (Drupal.settings.toolbar_hide.default) {
  $(document).ready(function(){$('#admin-menu').hide();$('body').addClass('adm_menu_hidden');});
}
$(document).keypress(function(e) {
	var unicode=e.keyCode? e.keyCode : e.charCode;
	if (String.fromCharCode(unicode)==Drupal.settings.toolbar_hide.key){
	  $('#admin-menu').slideToggle('fast');
	  $('body').toggleClass('tbr_menu_hidden');
	  //$('body.adm_menu_hidden').animate({margin-top: 'toggle'}, 5000);
	}
});
})(jQuery);
